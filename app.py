from flask import Flask
from Google import Create_Service
from googleapiclient.http import MediaFileUpload
import json

app = Flask(__name__)


@app.route("/")
def hello():
    path = './../cron/'
    with open(path + 'blog_data.json') as f:
        data = json.load(f)

    description = data['blog_content']['description'] + \
        "https://yuserver.in/"+data['slug']
    title = data['blog_content']['title']
    keywords = data['blog_content']['keywords']

    CLIENT_SECRET_FILE = 'client_file.json'
    API_NAME = 'youtube'
    API_VERSION = 'v3'
    SCOPES = ['https://www.googleapis.com/auth/youtube.upload']

    service = Create_Service(CLIENT_SECRET_FILE, API_NAME, API_VERSION, SCOPES)

    request_body = {
        'snippet': {
            'categoryI': 22,
            'title': title,
            'description': description,
            'tags': keywords.split(',')
        },
        'status': {
            'privacyStatus': 'private',
            'selfDeclaredMadeForKids': False,
        },
        'notifySubscribers': True
    }

    mediaFile = MediaFileUpload(path + 'output-video.mp4')

    response_upload = service.videos().insert(
        part='snippet,status',
        body=request_body,
        media_body=mediaFile
    ).execute()

    service.thumbnails().set(
        videoId=response_upload.get('id'),
        media_body=MediaFileUpload(path + 'image.png')
    ).execute()

    return data


if __name__ == "__main__":
    app.run()
